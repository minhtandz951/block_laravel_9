<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\v1\CategoryPostController;
use App\Models\CategoryPost;
use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $post = Post::with('categoryPost')->orderBy('id', 'DESC')->paginate(5);
        return view('layouts.post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = CategoryPost::pluck('title','id');
        return view('layouts.post.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $data = $request->all();
        $post->title = $data['title'];
        $post->short_desc = $data['short_desc'];
        $post->desc = $data['desc'];
        $post->view = $data['view'];
        $post->post_category_id = $data['category_id'];
        $post->created_at = Carbon::now('Asia/Ho_Chi_Minh');
        $post->updated_at = Carbon::now('Asia/Ho_Chi_Minh');

        $image = $request->file('image');
        if($image){

            $get_name_image = $image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = time().'_'.$name_image.'.'.$image->getClientOriginalExtension();
            $image->move(public_path('uploads'),$new_image);

            // $ext = $image->getClientOriginalExtension();
            // $name = time().'_'.$image->getClientOriginalName();
            // $image->move('uploads/',$name);
            $post->image = $new_image;
        }
        else{
            $post->image = 'default.jpg';
        }
        $post->save();
        return redirect()->route('post.index')->with('success', ' post Insert Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $category = CategoryPost::pluck('title','id');
        return view('layouts.post.show', compact('post','category'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $post = Post::find($id);
        $data = $request->all();
        $post->title = $data['title'];
        $post->short_desc = $data['short_desc'];
        $post->desc = $data['desc'];
        $post->view = $data['view'];
        $post->post_category_id = $data['category_id'];
        $post->updated_at = Carbon::now('Asia/Ho_Chi_Minh');

        $image = $request->file('image');
        if($image){
            if(file_exists('uploads/'.$post->image)){
                unlink('uploads/'.$post->image);
            }

            $get_name_image = $image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image = time().'_'.$name_image.'.'.$image->getClientOriginalExtension();
            $image->move(public_path('uploads'),$new_image);
            $post->image = $new_image;
        }

        $post->save();
        return redirect()->route('post.index')->with('success', ' post update Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        if(file_exists('uploads/'.$post->image)){
            unlink('uploads/'.$post->image);
        }
        $post->delete();
        return redirect()->back();
    }
}
