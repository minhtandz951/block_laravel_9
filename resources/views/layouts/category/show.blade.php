@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cập Nhập danh mục bài viết
                    <a href="{{route('category.index')}}">back</a>
                </div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                {!! Form::open(['route'=>['category.update',$category->id],'method'=>'PUT']) !!}
                    @csrf
                    <div class="form-group">
                        {!! Form::label('title', 'Tên danh mục', []) !!}
                        {!! Form::text('title', isset($category) ? $category->title : '', ['class'=>'form-control','placeholder'=>'Tiêu đề']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description', 'Mô tả ngắn', []) !!}
                        {!! Form::textarea('short_desc',isset($category) ? $category->short_desc : '', ['style'=>'resize:none','rows'=>'5', 'class'=>'form-control','id'=>'ckeditor_shortdesc']) !!}
                    </div>
                        {!! Form::submit('Cập Nhật Danh Mục', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
                 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
