@extends('layout')
@section('content')
@include('pages.banner')
<div class="about">
    <div class="container">
        <div class="about-main">
            <div class="col-md-8 about-left">
              
                <div class="about-tre">
                    <div class="a-1">
                        @foreach($all_post as $key => $post)
                            <div class="row" style="margin:5px">
                                <a href="{{route('bai-viet.show',[$post->id])}}">
                                    <div class="col-md-6 abt-left">
                                        <img src="{{asset('uploads/'.$post->image)}}" alt="{{Str::slug($post->title)}}" height = 200px />
                                    </div> 
                                    <div class="col-md-6 abt-left">
                                        <a href="{{route('danh-muc.show', [$post->post_category_id,  Str::slug($post->categoryPost->title)])}}">
                                            <h6>{{$post->categoryPost->title}}</h6>
                                         </a>
                                        <h3>  <a href="{{route('bai-viet.show',[$post->id])}}">{{$post->title}}</a></h3>
                                        <p>{!!$post->short_desc !!}.</p>
                                        <label>{{$post->updated_at}}</label>
                                        <div class="about-btn">
                                            <a href="{{route('bai-viet.show',[$post->id])}}">Đọc tiếp</a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        
                        <div class="clearfix"></div>
                    </div>
                </div>	
                {{$all_post->links('pagination::bootstrap-4')}}
            </div>
            <div class="col-md-4 about-right heading">
                <div class="abt-2">
                    <h3>Bài viết mới nhất</h3>
                    @foreach($new_post as $key => $newpost)
                    <a href="{{route('bai-viet.show', [$newpost->id])}}">
                        <div class="might-grid">
                            <div class="grid-might">
                               <img src="{{asset('uploads/'.$newpost->image)}}" class="img-responsive" alt="" > 
                            </div>
                            <div class="might-top">
                                <h4 ><a href="{{route('bai-viet.show', [$newpost->id])}}" style="font-size:15px" >{{$newpost->title}}</a></h4>
                                <p style="font-size:11px">{!!Str::substr($newpost->short_desc, 0, 100) !!} ...</p> 
                                <a href="{{route('bai-viet.show', [$newpost->id])}}" style="font-size:15px" >Đọc tiếp</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>	
                    </a>
                    @endforeach						
                </div>
                <div class="abt-2">
                    <h3>Bài viết xem nhiều</h3>
                    <ul>
                        @foreach($view_post as $key => $viewpost)
                        <li><a href="{{route('bai-viet.show', [$viewpost->id])}}">{{$viewpost->title}}</a></li>
                       @endforeach
                    </ul>	
                </div>
                <div class="abt-2">
                    <h3>NEWS LETTER</h3>
                    <div class="news">
                        <form>
                            <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" />
                            <input type="submit" value="Đăng kí">
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>			
        </div>		
    </div>
</div>
@endsection