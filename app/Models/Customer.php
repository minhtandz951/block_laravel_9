<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $timestamps = false;
    protected $fillable = ['name_customer',
    'phone_customer',
    'email_customer',
    'address_customer',
    'day_of_birth_customer'
    ];
    
    protected $primaryKey = 'id_customer';
    protected $table = 'customers';


    use HasFactory;
}
