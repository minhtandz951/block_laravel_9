<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Resources\v1\CustomerResources;
use App\Http\Resources\v1\CustomerCollection;
use Carbon\Carbon;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return Customer::all();
        return CustomerResources::collection(Customer::paginate());
        // return new CustomerCollection(Customer::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

     //Dùng GET
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     */
    // dùng POST
    public function store(Request $request)
    {
        $request->validate([
          'name_customer' => 'required', 
          'phone_customer' => 'required', 
          'email_customer' => 'required', 
          'address_customer' => 'required', 
          'day_of_birth_customer' => 'required'
          ]
        );

        $customer = Customer::create($request->all());
        return new CustomerResources($customer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //dùng GET
    public function show(Customer $customer)
    {
        return new CustomerResources($customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     //dùng PUT
    public function update(Request $request, Customer $customer)
    {
        $customer->update($request->all());
        return new CustomerResources($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //dùng DELETE
    public function destroy(Customer $customer)
    {
        $customer->delete();
    }
}
