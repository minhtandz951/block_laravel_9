<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



// Route::get('/customer',[customer::class, 'index'] );
//chỉ sài các phương thức vs only
// Route::resource('customer', customer::class)->only(['index', 'update', 'show', 'destroy', 'store']);

//không cai các phương thức vs except (k chạy đc đường dẫn)
// Route::resource('customer', customer::class)->except(['create', 'edit']);

// Route::resource('v1/customer', customer::class);


Route::prefix('v1')->group(function(){
    Route::resource('customer', customer::class)->only(['index', 'update', 'show', 'destroy', 'store']);

    Route::resource('category', App\Http\Controllers\API\v1\CategoryPostController::class);

    Route::resource('post', App\Http\Controllers\API\v1\PostController::class);

    Route::resource('bai-viet', App\Http\Controllers\API\v1\BaiVietController::class);

    Route::resource('danh-muc', App\Http\Controllers\API\v1\DanhMucController::class);
});



Route::prefix('v2')->group(function(){
    Route::resource('customer',  App\Http\Controllers\API\v2\CustomerController::class);
});
