<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CategoryPost;
use App\Models\Post;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_post = Post::with('categoryPost')->orderBy('id', 'DESC')->paginate(5);
        $category = CategoryPost::all();
        $new_post = Post::with('categoryPost')->orderBY('updated_at', 'DESC')->take(5)->get();
        $view_post = Post::with('categoryPost')->orderBY('view', 'DESC')->take(10)->get();
        return view('pages.main', compact('category','all_post','new_post','view_post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function tim_kiem(){
        $keywords = $_GET['keywords'];
        $category_post= Post::with('categoryPost')->orWhere('title', 'LIKE', '%'.$keywords.'%')->orWhere('short_desc', 'LIKE', '%'.$keywords.'%')->orWhere('desc', 'LIKE', '%'.$keywords.'%')->get();
        $category = CategoryPost::all();
        $view_post = Post::with('categoryPost')->orderBY('view', 'DESC')->take(7)->get();
        

        return view('pages.search', compact('category', 'category_post','keywords', 'view_post'));

    }

}
