@extends('layout')
@section('content')
<div class="single">
    <div class="container">
      <div class = "col-md-8">
          <div class="single-top">
                  {{-- <a href="#"><img class="img-responsive" src="{{asset('uploads/'.$post->image)}}" alt="{{Str::slug($post->title)}}"></a> --}}
              <div class=" single-grid">
                  <h4>{{$post->title}}</h4>				
                      <ul class="blog-ic">
                          <li><a href="#"><span> <i  class="glyphicon glyphicon-user"> </i>admin</span> </a> </li>
                          <li><span><i class="glyphicon glyphicon-time"> </i>{{$post->updated_at}}</span></li>		  						 	
                          <li><span><i class="glyphicon glyphicon-eye-open"> </i>view:{{$post->view}}</span></li>
                        </ul>	
                        {!!$post->desc !!}	  						
                  </div>

                  

              <div class="comments heading">
                  <h3>Bình luận về bài viết</h3>
                  {{-- <div class="media">
                        <div class="media-body">
                          <h4 class="media-heading">	Richard Spark</h4>
                          <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs .  </p>
                        </div>
                    <div class="media-right">
                      <a href="#">
                          <img src="images/si.png" alt=""> </a>
                    </div>
                  </div> --}}
                {{-- <div class="media">
                    <div class="media-left">
                      <a href="#">
                          <img src="images/si.png" alt="">
                      </a>
                    </div>
                    <div class="media-body">
                      <h4 class="media-heading">Joseph Goh</h4>
                      <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs .  </p>
                    </div>
                  </div>--}}
              </div> 
              <div class="comment-bottom heading">
                  <h3>Bình luận</h3>
                  <form>	
                  <input type="text" value="Name" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Name';}">
                  <input type="text" value="Email" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Email';}">
                  <input type="text" value="Subject" onfocus="this.value='';" onblur="if (this.value == '') {this.value ='Subject';}">
                  <textarea cols="77" rows="6" value=" " onfocus="this.value='';" onblur="if (this.value == '') {this.value = 'Message';}">Message</textarea>
                      <input type="submit" value="Gửi bình luận">
              </form>
              </div>
          </div>	
      </div>
      <div class="col-md-4 about-right heading">
        <div class="abt-2">
          <h3>Bài viết Liên quan</h3>
          @foreach($post_related as $key => $newpost)
          <a href="{{route('bai-viet.show', [$newpost->id])}}">
              <div class="might-grid">
                  <div class="grid-might">
                     <img src="{{asset('uploads/'.$newpost->image)}}" class="img-responsive" alt="" > 
                  </div>
                  <div class="might-top">
                      <h4 ><a href="{{route('bai-viet.show', [$newpost->id])}}" style="font-size:15px" >{{$newpost->title}}</a></h4>
                      <p style="font-size:11px">{!!Str::substr($newpost->short_desc, 0, 100) !!} ...</p> 
                      <a href="{{route('bai-viet.show', [$newpost->id])}}" style="font-size:15px" >Đọc tiếp</a>
                  </div>
                  <div class="clearfix"></div>
              </div>	
          </a>
          @endforeach						
      </div>
      </div>

    </div>					
</div>
@endsection