@extends('layouts.app')

@section('content')
<div class="container">



    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">danh mục bài viết
                    <a href="{{url('/home')}}">back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Tên danh mục</th>
                            <th scope="col">Mô tả danh mục</th>
                            <th scope="col">Hành động</th>
                            
                          </tr>
                        </thead>
                        <tbody class="order_position">
                          @foreach($category as $key => $cate)
                          <tr id="{{$cate->id}}">
                            <th scope="row">{{$key+1}}</th>
                            <td>{{$cate->title}}</td>
                            <td>{!!Str::substr($cate->short_desc, 0, 255)!!}</td>

                            <td>
                                {!! Form::open(['method'=>'DELETE','route'=>['category.destroy',$cate->id],'onsubmit'=>'return confirm("Bạn có chắc muốn xóa?")']) !!}
                                    {!! Form::submit('DELETE', ['class'=>'btn btn-danger btn-sm mb-2']) !!}
                                {!! Form::close() !!}
                              
                                <a href="{{route('category.show',$cate->id)}}" class="btn btn-warning btn-sm mb-2">EDIT</a>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                 
                 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
