@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Thêm bài viết
                    <a href="{{url('/home')}}">back</a>
                </div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {!! Form::open(['route'=>'post.store','method'=>'POST','enctype'=>'multipart/form-data']) !!}             
                    @csrf
                    
                    <div class="form-group">
                        {!! Form::label('title', 'Tiêu đề', []) !!}
                        {!! Form::text('title','', ['class'=>'form-control','placeholder'=>'Tiêu đề']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Category', 'Danh mục', []) !!}
                        {!! Form::select('category_id', $category, '', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('View', 'Thêm lược view', []) !!}
                        {!! Form::text('view', '', ['class'=>'form-control']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Image', 'Hình ảnh', []) !!}
                        {!! Form::file('image', ['class'=>'form-control-file']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Mô tả ngắn', []) !!}
                        {!! Form::textarea('short_desc',  '', ['style'=>'resize:none','rows'=>'5', 'class'=>'form-control','placeholder'=>'...','id'=>'ckeditor_shortdesc']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('desc', 'Mô tả ', []) !!}
                        {!! Form::textarea('desc', '', ['style'=>'resize:none','rows'=>'5',  'class'=>'form-control','placeholder'=>'...','id'=>'ckeditor_desc']) !!}
                    </div>
                        {!! Form::submit('Thêm Danh Mục', ['class'=>'btn btn-primary mt-2']) !!}
                   
                {!! Form::close() !!}
                 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
