<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;

use App\Models\CategoryPost;

use Illuminate\Http\Request;

use Session;

class CategoryPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = CategoryPost::orderBy('id', 'desc')->get();
        return view('layouts.category.index', compact('category'));
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new CategoryPost();
        $category->title = $request->title;
        $category->title = $request->short_desc;
        $category->save();
        return redirect()->route('category.index')->with('success', 'Category insert Successfully ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoryPost  $categoryPost
     * @return \Illuminate\Http\Response
     */
    public function show($categoryPost_id)
    {
        $category = CategoryPost::find($categoryPost_id);
        return view('layouts.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategoryPost  $categoryPost
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryPost $categoryPost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategoryPost  $categoryPost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoryPost_id )
    {
        $data = $request->all();
        $categoryPost = CategoryPost::find($categoryPost_id);

       
        
        $categoryPost->title = $data['title'];
        $categoryPost->short_desc54 = $data['short_desc'];
        $categoryPost->save();


        session()->put('status', 'category updated successfully');
        return redirect()->route('category.index')->with('message', 'Category Updated Successfully ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoryPost  $categoryPost
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoryPost_id)
    {
        $categoryPost = CategoryPost::find($categoryPost_id);
        $categoryPost->delete();
        return redirect()->back();
    }
}
