<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;
    use HasFactory;
    public function categoryPost(){
    	return $this->belongsTo(CategoryPost::class,'post_category_id');
    }

}
