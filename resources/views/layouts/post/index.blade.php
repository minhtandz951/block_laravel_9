@extends('layouts.app')

@section('content')
<div class="container" style="width:100%">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Bài viết
                    <a href="{{url('/home')}}">back</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table" style="width:100%">
                        <thead>
                          <tr>
                            <th scope="col">STT</th>
                            <th scope="col">Tiêu đề</th>
                            <th scope="col">Lượt xem</th>
                            <th scope="col">Hình ảnh</th>
                            <th scope="col">Mô tả ngắn</th>
                            <th scope="col">Thuộc danh mục</th>
                            <th scope="col">Ngày Thêm</th>
                            <th scope="col">Quản lý</th>
                          </tr>
                        </thead>
                        <tbody class="order_position">
                            @php
                                $i = 1;
                            @endphp
            
                          @foreach($post as $key => $p)
                          <tr id="{{$p->id}}">
                            <td scope="row">{{$i}}</td>
                            <td>{{$p->title}}</td>
                            <td>{{$p->view}}</td>
                            <td><img width="100" src="{{asset('uploads/'.$p->image)}}"></td>
                            <td>{!! Str::substr($p->short_desc, 0, 100) !!}</td>

                            <td>{{$p->categoryPost->title}}</td>
                            <td>{{$p->updated_at}}</td>

                            <td>
                                {!! Form::open(['method'=>'DELETE','route'=>['post.destroy',$p->id],'onsubmit'=>'return confirm("Bạn có chắc muốn xóa?")']) !!}
                                    {!! Form::submit('DELETE', ['class'=>'btn btn-danger btn-sm mb-2']) !!}
                                {!! Form::close() !!}
                                <a href="{{route('post.show',$p->id)}}" class="btn btn-warning btn-sm mb-2">EDIT</a>
                            </td>
                          </tr>
                          @php
                          $i = $i +1;
                      @endphp
                          @endforeach
                        </tbody>
                      </table>
                </div>
            </div>
            <div style="margin: 5px">
            {{$post->links('pagination::bootstrap-4')}}
            </div>

        </div>
    </div>
</div>
@endsection
