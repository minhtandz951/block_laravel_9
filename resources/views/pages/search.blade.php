@extends('layout')
@section('content')
<div class="about">
    <div class="container">
        <div class="about-main">
            <div class="col-md-8 about-left">
                <div class="about-one">  
                    <h3>Từ khóa tìm kiếm {{$keywords}}</h3>
                </div>
                
                <div class="about-tre">
                    <div class="a-1">
                        @foreach($category_post as $key => $post)
                            <div class="row" style="margin:5px">
                                <a href="{{route('bai-viet.show',[$post->id])}}">
                                    <div class="col-md-6 abt-left">
                                        <img src="{{asset('uploads/'.$post->image)}}" alt="{{Str::slug($post->title)}}" height = 200px />
                                    </div> 
                                    <div class="col-md-6 abt-left">
                                        <h6>{{$post->categoryPost->title}}</h6>
                                        <h3>  <a href="{{route('bai-viet.show',[$post->id])}}">{{$post->title}}</a></h3>
                                        <p>{!!$post->short_desc !!}.</p>
                                        <label>{{$post->updated_at}}</label>
                                        <div class="about-btn">
                                            <a href="{{route('bai-viet.show',[$post->id])}}">Đọc tiếp</a>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        
                        <div class="clearfix"></div>
                    </div>
                </div>	
            </div>
            <div class="col-md-4 about-right heading">
              
                <div class="abt-2">
                    <h3>Danh mục gợi ý</h3>
                    <ul>
                        @foreach($category as $key => $cate_recom)
                        <li><a href="{{route('danh-muc.show', [$cate_recom->id,  Str::slug($cate_recom->title)
                        ])}}">{{$cate_recom->title}}</a></li>
                        @endforeach   
                    </ul>	
                </div>
                <div class="abt-2">
                    <h3>Bài viết xem nhiều</h3>
                    @foreach($view_post as $key => $viewpost)
                    <a href="{{route('bai-viet.show', [$viewpost->id])}}">
                        <div class="might-grid">
                            <div class="grid-might">
                               <img src="{{asset('uploads/'.$viewpost->image)}}" class="img-responsive" alt="" > 
                            </div>
                            <div class="might-top">
                                <h4 ><a href="{{route('bai-viet.show', [$viewpost->id])}}" style="font-size:15px" >{{$viewpost->title}}</a></h4>
                                <p style="font-size:11px">{!!Str::substr($viewpost->short_desc, 0, 100) !!} ...</p> 
                                <a href="{{route('bai-viet.show', [$viewpost->id])}}" style="font-size:15px" >Đọc tiếp</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>	
                    </a>
                    @endforeach						
                </div>
                       						
                </div>     
            </div>
            <div class="clearfix"></div>			
        </div>		
    </div>
</div>
@endsection