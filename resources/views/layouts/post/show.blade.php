@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Thêm bài viết
                    <a href="{{url('/home')}}">back</a>
                </div>


                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {!! Form::open(['route'=>['post.update',$post->id],'method'=>'PUT','enctype'=>'multipart/form-data']) !!}             
                    @csrf
                    
                    <div class="form-group">
                        {!! Form::label('title', 'Tiêu đề', []) !!}
                        {!! Form::text('title',isset($post) ? $post->title : '', ['class'=>'form-control','placeholder'=>'Tiêu đề']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Category', 'Danh mục', []) !!}
                        {!! Form::select('category_id', $category, isset($post) ? $post->post_category_id : '', ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('view', 'Tiêu đề', []) !!}
                        {!! Form::text('view',isset($post) ? $post->view : '', ['class'=>'form-control','placeholder'=>'Tiêu đề']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('Image', 'Hình ảnh', []) !!}
                        {!! Form::file('image', ['class'=>'form-control-file']) !!}
                        @if(isset($post))
                        <img width="150" src="{{asset('uploads/'.$post->image)}}">
                      @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'Mô tả ngắn', []) !!}
                        {!! Form::textarea('short_desc',  isset($post) ? $post->short_desc : '',['style'=>'resize:none','rows'=>'5', 'class'=>'form-control','placeholder'=>'...','id'=>'ckeditor_shortdesc']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('desc', 'Mô tả ', []) !!}
                        {!! Form::textarea('desc', isset($post) ? $post->desc : '', ['style'=>'resize:none','rows'=>'5',  'class'=>'form-control','placeholder'=>'...','id'=>'ckeditor_desc']) !!}
                    </div>
                        {!! Form::submit('Cập nhập bài viết', ['class'=>'btn btn-primary mt-2']) !!}
                   
                {!! Form::close() !!}
                 

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
