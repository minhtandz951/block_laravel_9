<?php

class App extends Illuminate\Support\Facades\App {}
class Artisan extends Illuminate\Support\Facades\Artisan {}
class Auth extends Illuminate\Support\Facades\Auth {}
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController ;
use App\Http\Controllers\BaiVietController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',[HomeController::class , 'index'] );
Route::get('/bai-viet/{id}',[BaiVietController::class , 'show'] );
Auth::routes();

Route::get('/home', [LoginController::class, 'index'])->name('home');
Route::get('/tim-kiem', [HomeController::class, 'tim_kiem'])->name('tim-kiem');
